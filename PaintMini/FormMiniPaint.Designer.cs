﻿
namespace PaintMini
{
    partial class FormMiniPaint
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMiniPaint));
            this.mainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonChooseBackgroundColor = new System.Windows.Forms.Button();
            this.buttonChoosePaintColor = new System.Windows.Forms.Button();
            this.buttonPencil = new System.Windows.Forms.Button();
            this.buttonLine = new System.Windows.Forms.Button();
            this.buttonRectangle = new System.Windows.Forms.Button();
            this.buttonСircle = new System.Windows.Forms.Button();
            this.paintSpace = new PaintMini.PaintSpace();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.colorDialogBackground = new System.Windows.Forms.ColorDialog();
            this.colorDialogForeground = new System.Windows.Forms.ColorDialog();
            this.buttonClear = new System.Windows.Forms.Button();
            this.mainLayoutPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainLayoutPanel
            // 
            this.mainLayoutPanel.ColumnCount = 2;
            this.mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.Controls.Add(this.flowLayoutPanel1, 0, 1);
            this.mainLayoutPanel.Controls.Add(this.paintSpace, 1, 1);
            this.mainLayoutPanel.Controls.Add(this.menuStripMain, 0, 0);
            this.mainLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainLayoutPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.mainLayoutPanel.Name = "mainLayoutPanel";
            this.mainLayoutPanel.RowCount = 2;
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.Size = new System.Drawing.Size(949, 469);
            this.mainLayoutPanel.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.buttonClear);
            this.flowLayoutPanel1.Controls.Add(this.buttonChooseBackgroundColor);
            this.flowLayoutPanel1.Controls.Add(this.buttonChoosePaintColor);
            this.flowLayoutPanel1.Controls.Add(this.buttonPencil);
            this.flowLayoutPanel1.Controls.Add(this.buttonLine);
            this.flowLayoutPanel1.Controls.Add(this.buttonRectangle);
            this.flowLayoutPanel1.Controls.Add(this.buttonСircle);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 18);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(110, 451);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttonChooseBackgroundColor
            // 
            this.buttonChooseBackgroundColor.Location = new System.Drawing.Point(0, 21);
            this.buttonChooseBackgroundColor.Margin = new System.Windows.Forms.Padding(0);
            this.buttonChooseBackgroundColor.Name = "buttonChooseBackgroundColor";
            this.buttonChooseBackgroundColor.Size = new System.Drawing.Size(111, 21);
            this.buttonChooseBackgroundColor.TabIndex = 0;
            this.buttonChooseBackgroundColor.Text = "Цвет фона";
            this.buttonChooseBackgroundColor.UseVisualStyleBackColor = true;
            this.buttonChooseBackgroundColor.Click += new System.EventHandler(this.buttonChooseBackgroundColor_Click);
            // 
            // buttonChoosePaintColor
            // 
            this.buttonChoosePaintColor.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonChoosePaintColor.Location = new System.Drawing.Point(0, 42);
            this.buttonChoosePaintColor.Margin = new System.Windows.Forms.Padding(0);
            this.buttonChoosePaintColor.Name = "buttonChoosePaintColor";
            this.buttonChoosePaintColor.Size = new System.Drawing.Size(111, 21);
            this.buttonChoosePaintColor.TabIndex = 1;
            this.buttonChoosePaintColor.Text = "Цвет линии";
            this.buttonChoosePaintColor.UseVisualStyleBackColor = true;
            this.buttonChoosePaintColor.Click += new System.EventHandler(this.buttonChoosePaintColor_Click);
            // 
            // buttonPencil
            // 
            this.buttonPencil.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPencil.Location = new System.Drawing.Point(0, 63);
            this.buttonPencil.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPencil.Name = "buttonPencil";
            this.buttonPencil.Size = new System.Drawing.Size(111, 21);
            this.buttonPencil.TabIndex = 2;
            this.buttonPencil.Text = "Карандаш";
            this.buttonPencil.UseVisualStyleBackColor = true;
            this.buttonPencil.Click += new System.EventHandler(this.buttonPencil_Click);
            // 
            // buttonLine
            // 
            this.buttonLine.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonLine.Location = new System.Drawing.Point(0, 84);
            this.buttonLine.Margin = new System.Windows.Forms.Padding(0);
            this.buttonLine.Name = "buttonLine";
            this.buttonLine.Size = new System.Drawing.Size(111, 21);
            this.buttonLine.TabIndex = 3;
            this.buttonLine.Text = "Прямая линия";
            this.buttonLine.UseVisualStyleBackColor = true;
            this.buttonLine.Click += new System.EventHandler(this.buttonLine_Click);
            // 
            // buttonRectangle
            // 
            this.buttonRectangle.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonRectangle.Location = new System.Drawing.Point(0, 105);
            this.buttonRectangle.Margin = new System.Windows.Forms.Padding(0);
            this.buttonRectangle.Name = "buttonRectangle";
            this.buttonRectangle.Size = new System.Drawing.Size(111, 21);
            this.buttonRectangle.TabIndex = 4;
            this.buttonRectangle.Text = "Прямоугольник";
            this.buttonRectangle.UseVisualStyleBackColor = true;
            this.buttonRectangle.Click += new System.EventHandler(this.buttonRectangle_Click);
            // 
            // buttonСircle
            // 
            this.buttonСircle.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonСircle.Location = new System.Drawing.Point(0, 126);
            this.buttonСircle.Margin = new System.Windows.Forms.Padding(0);
            this.buttonСircle.Name = "buttonСircle";
            this.buttonСircle.Size = new System.Drawing.Size(111, 21);
            this.buttonСircle.TabIndex = 5;
            this.buttonСircle.Text = "Круг";
            this.buttonСircle.UseVisualStyleBackColor = true;
            this.buttonСircle.Click += new System.EventHandler(this.buttonСircle_Click);
            // 
            // paintSpace
            // 
            this.paintSpace.BackgroundColor = System.Drawing.Color.White;
            this.paintSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paintSpace.Location = new System.Drawing.Point(112, 18);
            this.paintSpace.Margin = new System.Windows.Forms.Padding(0);
            this.paintSpace.Name = "paintSpace";
            this.paintSpace.Size = new System.Drawing.Size(837, 451);
            this.paintSpace.TabIndex = 1;
            this.paintSpace.Load += new System.EventHandler(this.paintSpace_Load);
            // 
            // menuStripMain
            // 
            this.mainLayoutPanel.SetColumnSpan(this.menuStripMain, 2);
            this.menuStripMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFile,
            this.toolStripMenuItemEdit});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Padding = new System.Windows.Forms.Padding(0);
            this.menuStripMain.Size = new System.Drawing.Size(949, 18);
            this.menuStripMain.TabIndex = 2;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // toolStripMenuItemFile
            // 
            this.toolStripMenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemOpen,
            this.toolStripMenuItemSave,
            this.toolStripMenuItemSaveAs});
            this.toolStripMenuItemFile.Name = "toolStripMenuItemFile";
            this.toolStripMenuItemFile.Size = new System.Drawing.Size(48, 18);
            this.toolStripMenuItemFile.Text = "Файл";
            // 
            // toolStripMenuItemOpen
            // 
            this.toolStripMenuItemOpen.Name = "toolStripMenuItemOpen";
            this.toolStripMenuItemOpen.Size = new System.Drawing.Size(153, 22);
            this.toolStripMenuItemOpen.Text = "Открыть";
            this.toolStripMenuItemOpen.Click += new System.EventHandler(this.toolStripMenuItemOpen_Click);
            // 
            // toolStripMenuItemSave
            // 
            this.toolStripMenuItemSave.Name = "toolStripMenuItemSave";
            this.toolStripMenuItemSave.Size = new System.Drawing.Size(153, 22);
            this.toolStripMenuItemSave.Text = "Сохранить";
            this.toolStripMenuItemSave.Click += new System.EventHandler(this.toolStripMenuItemSave_Click);
            // 
            // toolStripMenuItemSaveAs
            // 
            this.toolStripMenuItemSaveAs.Name = "toolStripMenuItemSaveAs";
            this.toolStripMenuItemSaveAs.Size = new System.Drawing.Size(153, 22);
            this.toolStripMenuItemSaveAs.Text = "Сохранить как";
            this.toolStripMenuItemSaveAs.Click += new System.EventHandler(this.toolStripMenuItemSaveAs_Click);
            // 
            // toolStripMenuItemEdit
            // 
            this.toolStripMenuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemUndo,
            this.toolStripMenuItemRedo});
            this.toolStripMenuItemEdit.Name = "toolStripMenuItemEdit";
            this.toolStripMenuItemEdit.Size = new System.Drawing.Size(59, 18);
            this.toolStripMenuItemEdit.Text = "Правка";
            // 
            // toolStripMenuItemUndo
            // 
            this.toolStripMenuItemUndo.Name = "toolStripMenuItemUndo";
            this.toolStripMenuItemUndo.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemUndo.Text = "Отменить";
            this.toolStripMenuItemUndo.Click += new System.EventHandler(this.toolStripMenuItemUndo_Click);
            // 
            // toolStripMenuItemRedo
            // 
            this.toolStripMenuItemRedo.Name = "toolStripMenuItemRedo";
            this.toolStripMenuItemRedo.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemRedo.Text = "Вернуть";
            this.toolStripMenuItemRedo.Click += new System.EventHandler(this.toolStripMenuItemRedo_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(0, 0);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(111, 21);
            this.buttonClear.TabIndex = 6;
            this.buttonClear.Text = "Отчистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // FormMiniPaint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 469);
            this.Controls.Add(this.mainLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormMiniPaint";
            this.Text = "PaintMini";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.mainLayoutPanel.ResumeLayout(false);
            this.mainLayoutPanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttonChooseBackgroundColor;
        private System.Windows.Forms.Button buttonChoosePaintColor;
        private PaintSpace paintSpace;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpen;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSave;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSaveAs;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemUndo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRedo;
        private System.Windows.Forms.ColorDialog colorDialogBackground;
        private System.Windows.Forms.ColorDialog colorDialogForeground;
        private System.Windows.Forms.Button buttonPencil;
        private System.Windows.Forms.Button buttonLine;
        private System.Windows.Forms.Button buttonRectangle;
        private System.Windows.Forms.Button buttonСircle;
        private System.Windows.Forms.Button buttonClear;
    }
}