﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PaintMini
{
    using PaintMini.PaintObject;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using static PaintMini.PaintSpace;

    public partial class FormMiniPaint : Form
    {
        delegate Paint SelectedTool();
        SelectedTool selectedTool;

        String workSavePath = "";
        bool isEdit = false;
        enum PaintObjectEnum
        {
            pencil,
            line,
            rectangle,
            circle
        }
        private List<Paint> paintObjectStack;
        private int paintObjectStackIndex;
        private Color foregroundColor;
        public FormMiniPaint()
        {
            InitializeComponent();
            paintSpace.newPaint += PaintSpace_newPaint;
            foregroundColor = Color.Black;
            paintSpace.BackgroundColor = Color.White;
            paintObjectStack = new List<Paint>();
            paintObjectStackIndex = 0;
            selectedTool = SelectPencil;
            updateStateButtonsHistory();
        }

        private void PaintSpace_newPaint(object sender)
        {
            Paint activeObject = selectedTool();

            List<Paint> paintObjectStackTemp = new List<Paint>();
            for (int i = 0; i < paintObjectStackIndex; i++)
            {
                paintObjectStackTemp.Add(paintObjectStack[i]);
            }
            paintObjectStack = paintObjectStackTemp;

            paintObjectStack.Add(activeObject);
            paintObjectStackIndex++;

            paintSpace.PaintHandel(activeObject.Drawing, activeObject.Draw);
            updateStateButtonsHistory();

            isEdit = true;
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isEdit)
            {
                DialogResult result = MessageBox.Show("Сохранить изменения перед выходом?", "Paint Mini", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    e.Cancel = true;
                    Save(workSavePath);
                    e.Cancel = false;
                }
                if (result == DialogResult.Cancel) e.Cancel = true;
            }
        }

        private void buttonChooseBackgroundColor_Click(object sender, EventArgs e)
        {
            if (colorDialogBackground.ShowDialog() == DialogResult.OK) paintSpace.BackgroundColor = colorDialogBackground.Color;
        }



        private void FormMain_Load(object sender, EventArgs e)
        {

        }

        private void buttonChoosePaintColor_Click(object sender, EventArgs e)
        {
            if (colorDialogForeground.ShowDialog() == DialogResult.OK) foregroundColor = colorDialogForeground.Color;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            paintObjectStackIndex = 0;
            refrechPaintDrawStack();
            updateStateButtonsHistory();
        }

        private void buttonPencil_Click(object sender, EventArgs e)
        {
            selectedTool = SelectPencil;
        }

        private Paint SelectPencil()
        {
            return new Pencil(foregroundColor);
        }

        private void buttonLine_Click(object sender, EventArgs e)
        {
            selectedTool = SelectLine;
        }

        private Paint SelectLine()
        {
            return new Line(foregroundColor);
        }

        private void buttonRectangle_Click(object sender, EventArgs e)
        {
            selectedTool = SelectRectangle;
        }

        private Paint SelectRectangle()
        {
            return new Rectangle(foregroundColor);
        }

        private void buttonСircle_Click(object sender, EventArgs e)
        {
            selectedTool = SelectСircle;
        }

        private Paint SelectСircle()
        {
            return new Circle(foregroundColor);
        }

        private void toolStripMenuItemUndo_Click(object sender, EventArgs e)
        {
            if (paintObjectStack.Count == 0) return;
            paintObjectStackIndex--;
            refrechPaintDrawStack();
            updateStateButtonsHistory();
        }

        private void toolStripMenuItemRedo_Click(object sender, EventArgs e)
        {
            if (paintObjectStack.Count == paintObjectStackIndex) return;
            paintObjectStackIndex++;
            refrechPaintDrawStack();
            updateStateButtonsHistory();
        }

        private void refrechPaintDrawStack()
        {
            List<Draw> draws = new List<Draw>();
            for (int i = 0; i < paintObjectStackIndex; i++)
            {
                draws.Add(paintObjectStack[i].Draw);
            }
            paintSpace.SetDrawsList(draws);
        }

        private void updateStateButtonsHistory()
        {
            if (paintObjectStack.Count == 0)
            {
                toolStripMenuItemUndo.Enabled = false;
                toolStripMenuItemRedo.Enabled = false;
                return;
            }
            if (paintObjectStackIndex >= paintObjectStack.Count) toolStripMenuItemRedo.Enabled = false;
            else toolStripMenuItemRedo.Enabled = true;
            if (paintObjectStackIndex <= 0) toolStripMenuItemUndo.Enabled = false;
            else toolStripMenuItemUndo.Enabled = true;
        }

        private void paintSpace_Load(object sender, EventArgs e)
        {

        }

        private void Save(String path = "")
        {
            BinaryFormatter formatter = new BinaryFormatter();
            if (path == "")
            {
                SaveFileDialog fileDialog = new SaveFileDialog();
                fileDialog.Filter = "PaintMini files (*.mpic)|*.mpic|All files (*.*)|*.*";
                fileDialog.ShowDialog();
                path = fileDialog.FileName;

            }

            if (path != "") using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                    formatter.Serialize(fs, paintSpace.BackgroundColor);
                    formatter.Serialize(fs, paintObjectStackIndex);
                    for (int i = 0; i < paintObjectStack.Count; i++)
                    {
                        formatter.Serialize(fs, paintObjectStack[i]);
                    }
            }
            workSavePath = path;
        }

        private void Open()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "PaintMini files (*.mpic)|*.mpic|All files (*.*)|*.*";
            fileDialog.ShowDialog();
            String path = fileDialog.FileName;

            if (path != "") using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                paintObjectStack.Clear();
                paintSpace.BackgroundColor = (Color)formatter.Deserialize(fs);
                paintObjectStackIndex = (int)formatter.Deserialize(fs);
                while (fs.Length != fs.Position)
                {
                    paintObjectStack.Add((Paint)formatter.Deserialize(fs));
                }
                
                refrechPaintDrawStack();
                updateStateButtonsHistory();
            }
            isEdit = false;
        }

        private void toolStripMenuItemOpen_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void toolStripMenuItemSave_Click(object sender, EventArgs e)
        {
            Save(workSavePath);
        }

        private void toolStripMenuItemSaveAs_Click(object sender, EventArgs e)
        {
            Save();
        }
    }
}
