﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace PaintMini.PaintObject
{
    [Serializable]
    public class Pencil : Paint
    {
        private List<Point> points;
        public Pencil (Color color, float width = 1) : base(color, width)
        {
            points = new List<Point>();
        }

        public Pencil() : base()
        {
            points = new List<Point>();
        }

        public override void Draw(Graphics g)
        {
            Pen pen = new Pen(colorPen, widthPen);
            for (int i = 1; i < points.Count; i++)
            {
                g.DrawLine(pen, points[i-1], points[i]);
            }
        }

        public override void Drawing(Point point1, Point point2)
        {
            points.Add(point2);
        }
    }
}
