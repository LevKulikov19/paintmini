﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace PaintMini.PaintObject
{
    [Serializable]
    class Circle : Paint
    {
        private Point pointStart;
        private Point pointEnd;
        public Circle(Color color, float width = 1) : base(color, width)
        {
            pointStart = new Point();
            pointEnd = new Point();
        }

        public override void Draw(Graphics g)
        {
            Pen pen = new Pen(colorPen, widthPen);
            g.DrawEllipse(pen, pointStart.X, pointStart.Y,
                pointEnd.X - pointStart.X, pointEnd.X - pointStart.X);
        }

        public override void Drawing(Point point1, Point point2)
        {
            pointStart = point1;
            pointEnd = point2;
        }
    }
}
