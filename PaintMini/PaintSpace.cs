﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PaintMini
{
    public partial class PaintSpace : UserControl
    {
        public delegate void NewPaint(object sender);
        public event NewPaint newPaint;
        private bool isMouseDown = false;
        private Point pointStart;
        private Point pointEnd;
        public delegate void Drawing(Point point1, Point point2);
        Drawing drawing;
        public delegate void Draw(Graphics g);
        Draw draw;
        private List<Draw> layers;
        public List<Draw> GetDrawsList ()
        {
            return layers;
        }

        public void SetDrawsList(List<Draw> value)
        {
            layers = value;
            RefreshPaintSpace();
        }

        private Color backgroundColor;
        public Color BackgroundColor
        {
            get => backgroundColor;
            set
            {
                backgroundColor = value;
                RefreshPaintSpace();
            }
        }

        public PaintSpace()
        {
            InitializeComponent();
            backgroundColor = Color.White;
            layers = new List<Draw>();
        }

        private void PaintSpace_Load(object sender, EventArgs e)
        {

        }

        private void PaintSpace_Paint(object sender, PaintEventArgs e)
        {
            RefreshPaintSpace();

        }

        private void RefreshPaintSpace()
        {
            Graphics graphicsPaintSpace = this.CreateGraphics();
            BufferedGraphicsContext bufferedGraphicsContext = BufferedGraphicsManager.Current;
            BufferedGraphics bufferedGraphics = bufferedGraphicsContext.Allocate(graphicsPaintSpace, this.DisplayRectangle);
            Graphics gPaint = bufferedGraphics.Graphics;
            
            gPaint.Clear(backgroundColor);
            gPaint.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            foreach (Draw draw in layers)
            {
                draw(gPaint);
            }

            bufferedGraphics.Render(graphicsPaintSpace);
        }

        public void PaintHandel(Drawing paint, Draw d)
        {
            drawing = new Drawing(paint);
            draw = new Draw(d);
            layers.Add(draw);
        }

        private int tick = 0;
        private void PaintSpace_MouseMove(object sender, MouseEventArgs e)
        {
            if (tick % 2 == 0)
            {
                pointEnd = e.Location;
                if (isMouseDown)
                {
                    drawing(pointStart, pointEnd);
                    RefreshPaintSpace();
                }
            }

            tick++;
        }

        private void PaintSpace_MouseDown(object sender, MouseEventArgs e)
        {
            newPaint(this);
            isMouseDown = true;
            pointStart = e.Location;
        }

        private void PaintSpace_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            pointEnd = e.Location;
        }
    }
}
