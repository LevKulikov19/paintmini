﻿using PaintMini.PaintObject;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace PaintMini
{
    [XmlInclude(typeof(Pencil))]
    [Serializable]
    public abstract class Paint
    {
        protected Color colorPen;
        protected float widthPen;
        public Paint(Color color, float width = 1)
        {
            colorPen = color;
            widthPen = width;
        }
        public Paint()
        {
            colorPen = Color.Black;
            widthPen = 1;
        }
        public abstract void Drawing(Point point1, Point point2);
        public abstract void Draw(Graphics g);
    }
}
